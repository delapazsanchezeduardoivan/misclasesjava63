/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empleados;

/**
 *
 * @author delap
 */
public class EmpleadoEventual extends Empleados {
    private float pagoHora;
    private float horasTrabajadas;
    
    //Constructores.

    public EmpleadoEventual(float pagoHora, float horasTrabajadas, int numEmpleados, String nombre, String puesto, String departamento) {
        super(numEmpleados, nombre, puesto, departamento);
        this.pagoHora = pagoHora;
        this.horasTrabajadas = horasTrabajadas;
    }
    // Get and Set

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(float horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    @Override
    public float calcularPago() {
        return this.horasTrabajadas * this.pagoHora;
    }
}
