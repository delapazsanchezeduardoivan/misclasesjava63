/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empleados;

/**
 *
 * @author delap
 */
public abstract class Empleados {
    protected int numEmpleados;
    protected String nombre;
    protected String puesto;
    protected String departamento;
    
    //Constructores.

    public Empleados(int numEmpleados, String nombre, String puesto, String departamento) {
        this.numEmpleados = numEmpleados;
        this.nombre = nombre;
        this.puesto = puesto;
        this.departamento = departamento;
    }
    public Empleados(){
        this.numEmpleados = 0;
        this.nombre = "";
        this.puesto = "";
        this.departamento = "";
    }
    //Encapsulamiento.

    public int getNumEmpleados() {
        return numEmpleados;
    }

    public void setNumEmpleados(int numEmpleados) {
        this.numEmpleados = numEmpleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    public abstract float calcularPago();
        
}
