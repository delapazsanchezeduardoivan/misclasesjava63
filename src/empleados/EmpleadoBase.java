/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package empleados;

/**
 *
 * @author delap
 */
public class EmpleadoBase extends Empleados implements impuestos{
    private float pagoDiario;
    private float diasTrabajados;
    
    //Constructor.

    public EmpleadoBase(float pagoDiario, float diasTrabajados, int numEmpleados, String nombre, String puesto, String departamento) {
        super(numEmpleados, nombre, puesto, departamento);
        this.pagoDiario = pagoDiario;
        this.diasTrabajados = diasTrabajados;
    }
    public EmpleadoBase(){
        super();
        this.diasTrabajados = 0.0f;
        this.pagoDiario = 0.0f;
        
    }
    //Get and Set
    
    public float getPagoDiario() {
        return pagoDiario;
    }

    public void setPagoDiario(float pagoDiario) {
        this.pagoDiario = pagoDiario;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    

    @Override
    public float calcularPago() {
      return this.diasTrabajados * this.pagoDiario;
    }

    @Override
    public float calcularImpuestos() {
       float pago = 0;
       if (this.calcularPago()>= 15000) pago = this.calcularPago()*.15f;
       return pago;
       
    }
    

}
